<?php


namespace Pallino\ContactForm\Controller;


use Pallino\ContactForm\Service\CsrfMetaTagManager;
use Pallino\ContactForm\Service\MailGeneratorService;
use Pallino\ContactForm\Service\ValidationService;
use Psr\Log\LoggerAwareTrait;
use Symfony\Component\Mime\Address;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManager;
use TYPO3\CMS\Core\Mail\FluidEmail;
use TYPO3\CMS\Core\Mail\Mailer;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Error\Error;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Extbase\Validation\Validator\ValidatorInterface;

class ContactFormController extends ActionController implements \Psr\Log\LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var \TYPO3\CMS\Extbase\Mvc\View\JsonView
     */
    protected $view;

    /**
     * @var CsrfMetaTagManager
     */
    protected $csrfMetaTagManager;

    /**
     * @var array
     */
    protected $formConfiguration = [];

    /**
     * @var array
     */
    protected $errors = [];

    /**
     * @var ValidationService
     */
    protected $validationService;

    /**
     * @var MailGeneratorService
     */
    protected $mailGeneratorService;

    /**
     * @var string
     */
    protected $defaultViewObjectName = \TYPO3\CMS\Extbase\Mvc\View\JsonView::class;

    public function injectCsrfTokenManager(CsrfTokenManager $csrfTokenManager)
    {
        $this->csrfTokenManager = $csrfTokenManager;
    }

    public function injectCsrfMetaTagManager(CsrfMetaTagManager $csrfMetaTagManager)
    {
        $this->csrfMetaTagManager = $csrfMetaTagManager;
    }

    public function injectValidationService(ValidationService $validationService)
    {
        $this->validationService = $validationService;
    }

    public function injectMailGeneratorService(MailGeneratorService $mailGeneratorService)
    {
        $this->mailGeneratorService = $mailGeneratorService;
    }

    public function makeCsrfTagAction()
    {
        $this->response->addAdditionalHeaderData($this->csrfMetaTagManager->getMetaTag($this->settings['csrfToken']));
        return '';
    }

    protected function isError()
    {
        return count($this->errors) > 0;
    }

    protected function successResult(array $sanitizedData)
    {
        $data['output']['result'] = LocalizationUtility::translate('form.successfully_submitted','contact_form');
        $data['output']['data'] = $sanitizedData;
        $this->assignEmailTemplateVariables($data);
        $this->mailGeneratorService->setConfiguration($this->settings);
        $this->mailGeneratorService->sendEmails($sanitizedData);
    }

    protected function failedResult(array $sanitizedData)
    {
        $data['output']['result'] = LocalizationUtility::translate('form.failed_submitted','contact_form');
        $data['output']['data'] = $sanitizedData;
        $data['output']['errors'] = $this->validationService->getErrors();
        $this->assignEmailTemplateVariables($data);
    }

    public function sendContactAction()
    {
        $this->blockDisallowedHttpMethod();
        $typo3Request = $GLOBALS['TYPO3_REQUEST'] ?? null;
        /** @var \TYPO3\CMS\Core\Http\ServerRequest $typo3Request */
        if ($jsonBody = $typo3Request->getBody()) {
            if (($data = json_decode($jsonBody, true)) == NULL) {
                $this->throwStatus(400, null, 'Bad Request: incorrect body request.');
            }
            $tokenToValidate = $this->getRequestCsrfToken($typo3Request);
            if ($this->csrfTokenManager->isTokenValid($tokenToValidate)) {
                $this->processAfterTokenValidationIsSuccessfully($data);
            } else {
                $this->throwStatus(400, null, 'Bad Request: token invalid.');
            }
        } else {
            $this->throwStatus(400, null, 'Bad Request.');
        }
    }

    /**
     * @return array
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     */
    protected function blockDisallowedHttpMethod(): bool
    {
        switch ($this->request->getMethod()) {
            case 'POST':
                return true;
            case 'HEAD':
            case 'GET':
            case 'PUT':
            case 'DELETE':
            default:
                $this->throwStatus(400, null, 'Bad Request.');
        }
    }

    /**
     * @param \TYPO3\CMS\Core\Http\ServerRequest|null $typo3Request
     * @return CsrfToken
     */
    protected function getRequestCsrfToken(?\TYPO3\CMS\Core\Http\ServerRequest $typo3Request): CsrfToken
    {
        $tokenApiName = $this->settings['csrfToken']['identifier'] ?? '';
        $csrfTokenValue = $typo3Request->getHeader('x-csrf-token')[0] ?? null;
        $tokenToValidate = new CsrfToken($tokenApiName, $csrfTokenValue);
        return $tokenToValidate;
    }

    protected function getFormFieldsFromSettings(): void
    {
        if ($this->settings['form']['fields'] && count($this->settings['form']['fields'])) {
            foreach ($this->settings['form']['fields'] as $name => $configuration) {
                $this->formConfiguration['fields'][] = $name;
                $this->formConfiguration['configuration'][$name] = $configuration;
                if (!isset($this->formConfiguration['configuration'][$name]['required'])) {
                    $this->formConfiguration['configuration'][$name]['required'] = 0;
                }
            }
        }
    }

    protected function sanitizeFormField(array $requestData): array
    {
        $sanitizedFields = [];
        foreach ($requestData as $name => $value) {
            if (in_array($name, $this->formConfiguration['fields'])) {
                $sanitizedFields[$name] = $value;
            }
        }
        return $sanitizedFields;
    }

    /**
     * @param $data
     */
    protected function assignEmailTemplateVariables($data): void
    {
        $this->view->setVariablesToRender(['output']);
        $this->view->assignMultiple($data);
    }

    /**
     * @param $data
     */
    protected function processAfterTokenValidationIsSuccessfully($data): void
    {
        $this->getFormFieldsFromSettings();
        $this->validationService->setConfiguration($this->formConfiguration);
        $sanitizedData = $this->validationService->getSanitizeFormFields($data);
        if($this->validationService->isPresentRequiredFields($data)){}
        try {
            if($this->validationService->evaluateForm($sanitizedData)){
                $this->successResult($sanitizedData);
            }
            else{
                $this->failedResult($sanitizedData);
            }
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());
            $this->throwStatus(500, null, 'Validation Process Exception.');
        }


    }

    /**
     * @param $validatorClass
     * @param array $options
     * @param $value
     * @param $name
     * @param $validatorName
     */
    protected function validateAndFetchErrors($validatorClass, array $options, $value, $name, $validatorName): void
    {
        /** @var ValidatorInterface $validatorObject */
        $validatorObject = GeneralUtility::makeInstance($validatorClass, $options);
        $result = $validatorObject->validate($value);
        if ($result->hasErrors()) {
            /** @var Error $error */
            foreach ($result->getErrors() as $error)
                $this->errors[$name][$validatorName] = $error->getMessage();
        }
    }


}