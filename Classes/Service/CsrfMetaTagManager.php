<?php
declare(strict_types = 1);

namespace Pallino\ContactForm\Service;

use Symfony\Component\Security\Csrf\CsrfTokenManager;

class CsrfMetaTagManager
{
    /**
     * @var CsrfTokenManager
     */
    protected $csrfTokenManager;

    public function injectCsrfTokenManager(CsrfTokenManager $csrfTokenManager)
    {
        $this->csrfTokenManager = $csrfTokenManager;
    }

    /**
     * @param array $tokenInfo contains identifier and name keys
     * @return string meta tag to embedded into page
     * @throws \Exception in case argument miss
     */
    public function getMetaTag($tokenInfo): string
    {
        if(!isset($tokenInfo['identifier']) || !isset($tokenInfo['name'])){
            throw new \Exception('identifier or name is missing in token request');
        }
        $identifier = (string)$tokenInfo['identifier'];
        $name = (string)$tokenInfo['name'];
        $token = $this->csrfTokenManager->getToken($identifier);
        return sprintf('<meta name="%s" content="%s">',$name,$token->getValue());
    }
}