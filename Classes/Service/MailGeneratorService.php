<?php
declare(strict_types=1);


namespace Pallino\ContactForm\Service;


use Symfony\Component\Mime\Address;
use TYPO3\CMS\Core\Mail\FluidEmail;
use TYPO3\CMS\Core\Mail\Mailer;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class MailGeneratorService
{
    /**
     * @var array
     */
    protected $formConfiguration;

    protected function getFormConfiguration($key = null): array
    {

        if (!isset($this->formConfiguration[$key])) {
            throw new \Exception('missing configuration array');
        }
        return $this->formConfiguration[$key];
    }

    public function setConfiguration(array $configuration): void
    {
        $this->formConfiguration = $configuration;
    }


    public function sendEmails($data)
    {
        $variables = $this->getEmailInformation($data);
        $this->sendTYPO3Email($variables);
    }

    protected function getEmailInformation($data) : array
    {
        $mailVariables = [];
        $mailVariables['senderName'] = '';
        $configuration = $this->getFormConfiguration('mail');
        if(isset($configuration['sender']['name'])){
            $mailVariables['senderName'] = $this->getValueFromConfiguration($configuration['sender']['name'],$data);
        }
        $mailVariables['senderEmail'] = $this->getValueFromConfiguration($configuration['sender']['mail'],$data);
        $mailVariables['receiver'] = $this->getValueFromConfiguration($configuration['receiver'],$data);
        if(isset($configuration['greetings']['enable']) && $configuration['greetings']['enable'] == 1){
            $mailVariables['GreetingsSenderName'] = '';
            if(isset($configuration['greetings']['sender']['name'])){
                $mailVariables['GreetingsSenderName'] = $this->getValueFromConfiguration($configuration['greetings']['sender']['name'],$data);
            }
            $mailVariables['GreetingsSenderEmail'] = $this->getValueFromConfiguration($configuration['greetings']['sender']['mail'],$data);
            $mailVariables['GreetingsSenderReceiver'] = $this->getValueFromConfiguration($configuration['greetings']['sender']['receiver'],$data);
        }
        return array_merge($mailVariables,$data);
    }

    protected function getValueFromConfiguration($configuration,$data = []) : string
    {
        if(isset($configuration['value'])){
            return $configuration['value'];
        }
        elseif(isset($configuration['field'])){
            return $data[$configuration['field']];
        }
        else{
            return '';
        }
    }

    protected function sendTYPO3Email($mailData)
    {
        /** @var FluidEmail $email */
        $email = $this->getEmailFluidObject();
        $configuration = $this->getFormConfiguration('mail');
        $email->to($mailData['receiver'])
            ->from(new Address($mailData['senderEmail'], $mailData['senderName']))
            ->subject($this->getValueFromConfiguration($configuration['subject'],$mailData));
        $email->setTemplate($configuration['contactFormTemplate']);
        $email->assignMultiple($mailData);
        $email->format('html');
        $this->completeEmailSendingProcess($email);
    }

    protected function getEmailFluidObject()
    {
        return GeneralUtility::makeInstance(FluidEmail::class);
    }

    protected function completeEmailSendingProcess(FluidEmail $email)
    {
        GeneralUtility::makeInstance(Mailer::class)->send($email);
    }
}