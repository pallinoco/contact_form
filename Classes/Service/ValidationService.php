<?php
declare(strict_types=1);


namespace Pallino\ContactForm\Service;


use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Error\Error;
use TYPO3\CMS\Extbase\Validation\Validator\ValidatorInterface;

class ValidationService
{
    /**
     * @var array
     */
    protected $formConfiguration;

    /**
     * @var array
     */
    protected $missingRequiredFields = [];

    /**
     * @var array
     */
    protected $errors = [];

    protected function getFormConfiguration($key = null): array
    {

        if (!isset($this->formConfiguration[$key])) {
            throw new \Exception('missing configuration array');
        }
        return $this->formConfiguration[$key];
    }

    protected function setRequiredMissingFields(array $requestData): array
    {
        $this->missingRequiredFields = [];
        foreach ($this->getFormConfiguration('configuration') as $name => $configuration) {
            if (!array_key_exists($name, $requestData) && $configuration['required'] == 1) {
                $this->missingRequiredFields[] = $name;
            }
        }
        return $this->missingRequiredFields;
    }

    public function setConfiguration(array $configuration): void
    {
        $this->formConfiguration = $configuration;
    }

    public function getMissingRequiredFields(): array
    {
        return $this->missingRequiredFields;
    }

    public function isPresentRequiredFields(array $requestData): bool
    {
        return count($this->setRequiredMissingFields($requestData)) == 0;
    }

    public function getSanitizeFormFields(array $formData): array
    {
        $sanitizedFields = [];
        foreach ($formData as $name => $value) {
            if (in_array($name, $this->getFormConfiguration('fields'))) {
                $sanitizedFields[$name] = $value;
            }
        }
        return $sanitizedFields;
    }

    public function evaluateForm(array $requestSanitizedData): bool
    {
        $configuration = $this->getFormConfiguration('configuration');
        foreach ($requestSanitizedData as $name => $value) {
            if (isset($configuration[$name]['validators'])) {
                foreach ($configuration[$name]['validators'] as $validator) {
                    $this->validateWithSpecificValidator($validator, $value, $name);
                }
            }
        }
        return count($this->errors) == 0;
    }

    /**
     * @param $validator
     * @param $value
     * @param $name
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     */
    protected function validateWithSpecificValidator($validator, $value, $name): void
    {
        $options = [];
        $validatorName = '';
        if (is_array($validator)) {
            list($validatorName, $validatorClass, $options) = $this->getValidatorInformation($validator, $options);
        } else {
            $validatorClass = $validator;
        }
        if ($validatorName == '') {
            $validatorName = $validatorClass;
        }
        $this->validateAndFetchErrors($validatorClass, $options, $value, $name, $validatorName);
    }

    /**
     * @param array $validator
     * @param array $options
     * @return array
     */
    protected function getValidatorInformation(array $validator, array $options): array
    {
        $validatorName = '';
        foreach ($validator as $key => $item) {
            switch ($key) {
                case '_typoScriptNodeValue':
                    $validatorClass = $validator['_typoScriptNodeValue'];
                    break;
                case 'alias':
                    $validatorName = $validator['alias'];
                    break;
                default:
                    $options[$key] = $item;
            }
        }
        return array($validatorName, $validatorClass, $options);
    }

    /**
     * @param $validatorClass
     * @param array $options
     * @param $value
     * @param $name
     * @param $validatorName
     */
    protected function validateAndFetchErrors($validatorClass, array $options, $value, $name, $validatorName): void
    {
        $validatorObject = $this->getValidatorObject($validatorClass, $options);
        $result = $validatorObject->validate($value);
        if ($result->hasErrors()) {
            /** @var Error $error */
            foreach ($result->getErrors() as $error)
                $this->errors[$name][$validatorName] = $error->getMessage();
        }
    }

    protected function getValidatorObject($validatorClass, array $options): ValidatorInterface
    {
        return GeneralUtility::makeInstance($validatorClass, $options);
    }

    public function getErrors()
    {
        return $this->errors;
    }

}