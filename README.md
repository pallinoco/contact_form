Configurazione
La configurazione avviene via typoscript:

```typo3_typoscript
plugin.tx_contactform_pi1{
    settings {
        csrfToken{
            identifier = contactformToken
            name = x-csrf-token
        }
        mail {
            contactFormTemplate = ContactFormCustomer
            sender.mail.field = email
            receiver.field = email
            greetings {
                enable = 1
                receiver.field = email
                sender.name.value = federico
                sender.email.value = federico@bernardin.it
            }
        }
        form {
            fields {
                name {
                    validators {
                        1 = TYPO3\CMS\Extbase\Validation\Validator\StringLengthValidator
                        1.minimum = 4
                        1.maximum = 15
                        #1.alias = name
                    }
                    required = 1
                }
                email {
                    validators {
                        1 = TYPO3\CMS\Extbase\Validation\Validator\EmailAddressValidator
                        1.alias = email
                    }
                    required = 1
                }
                surname {
                    required = 0
                }
            }
        }
    }
}

lib.csrfToken = USER_INT
lib.csrfToken {
    userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
    extensionName = ContactForm
    pluginName = Pi1
    vendorName = Pallino

    action = makeCsrfTag
    switchableControllerActions {
        ContactForm {
            1 = makeCsrfTag
        }
    }
    settings < plugin.tx_contactform.settings
}

```
Il primo blocco di codice definisce le configurazioni generali sia per il form con le sue validazioni, sia per l'invio delle mail.
I validatori sono definiti in formato array. Il campo obbligatorio è il FQDN della classe, gli eeventuali argomenti del validatore ed un campo alias che sarà la chiave del json di risposta se ci dovessero essere errori.
Il secondo blocco di codice contiene, invece, la parte di controllo del csrf all'interno del tag meta.

L'output differisce se ci sono errori o meno. Se ci sono errori sarà presente una chiave errors che contiene per ogni campo la lista delle validazioni fallite.
Per identificare la voce si usa la chiave alias o se non specificata il percorso della classe del validatore.
In ogni caso i dati inviati e sanificati sono reinviati come risposta.
Nel campo result è presente una breve descrizione del risultato, come ad esempio "Ci sono degli errori" oppure "Grazie per averci contattato sarai ricntattao appena possibile".

```json5
{
  "result": "Form submitted successfully",
  "data": {
    "email": "federico.bernardin@pallino.it",
    "name": "Mario"
  }
}
```

```json5
{
  "result": "error in fields",
  "data": {
    "email": "federico.bernardin",
    "name": "Mariodsfsdfdsfsdafsdfsfdsfdsfsdafsdfsd"
  },
  "errors": {
    "email": {
      "email": "field incorrect"
    },
    "name": {
      "TYPO3\\CMS\\Extbase\\Validation\\Validator\\StringLengthValidator": "field incorrect"
    }
  }
}
```