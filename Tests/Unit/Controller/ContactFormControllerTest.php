<?php
declare(strict_types = 1);

namespace Pallino\ContactForm\Tests\Unit\Controller;
use Pallino\ContactForm\Controller\ContactFormController;
use Pallino\ContactForm\Service\CsrfMetaTagManager;
use PHPUnit\Framework\MockObject\MockObject;
use Prophecy\Prophecy\ObjectProphecy;
use TYPO3\CMS\Extbase\Mvc\Response;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

class ContactFormControllerTest extends UnitTestCase
{

    protected $response;

    protected $controller;

    protected function setUp() : void
    {
        parent::setUp();
        $this->response = $this->prophesize(Response::class);
        $this->controller = $this->getAccessibleMock(ContactFormController::class,null);
    }

    protected function getTokenManager() :  ObjectProphecy
    {
        return $this->getMockBuilder(CsrfMetaTagManager::class)->addMethods(['getMetaTag'])->getMock();

    }


    protected function getCsrfMetaTagManager() :  MockObject
    {
        return $this->getMockBuilder(CsrfMetaTagManager::class)->onlyMethods(['getMetaTag'])->getMock();

    }


    public function testMakeCsrfTagSuccesfully(): void
    {
        /** @var MockObject|CsrfMetaTagManager $csrfMetaTagManager */
        $csrfMetaTagManager = $this->getCsrfMetaTagManager();
        $csrfMetaTagManager->method('getMetaTag')->willReturn('<meta name="test" content="fakeToken">');
        $this->controller->injectCsrfMetaTagManager($csrfMetaTagManager);
        $this->response->addAdditionalHeaderData('<meta name="test" content="fakeToken">')->shouldBeCalledTimes(1);
        $this->controller->_set('response',$this->response->reveal());
        $this->controller->_set('settings',array('csrfToken' => ''));
        $this->controller->makeCsrfTagAction();
    }
}
