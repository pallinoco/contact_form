<?php

namespace Pallino\ContactForm\Tests\Unit\Service;

use Pallino\ContactForm\Service\CsrfMetaTagManager;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManager;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

class CsrfMetaTagManagerTest extends UnitTestCase
{
    /**
     * @var CsrfMetaTagManager
     */
    protected $service;

    /**
     * @var CsrfTokenManager
     */
    protected $csrfTokenManager;

    protected function setUp() : void
    {
        parent::setUp();
        $this->service = $this->getAccessibleMock(CsrfMetaTagManager::class,null);
    }

    protected function getCsrfTokenManager(CsrfToken $token)
    {
        $this->csrfTokenManager = $this->getMockBuilder(CsrfTokenManager::class)->onlyMethods(['getToken'])->getMock();
        $this->csrfTokenManager->expects($this->once())->method('getToken')-> willReturn($token);
        return $this->csrfTokenManager;
    }

    public function testSettingsKeyMissedGenerateException()
    {
        $settings = ['identifier' => ''];
        $this->expectException(\Exception::class);
        $this->service->getMetaTag($settings);
        $settings = ['name' => ''];
        $this->service->getMetaTag($settings);
    }

    public function testMetaTagCreatedSuccessfully()
    {
        $settings = [
            'identifier' => 'testIdentifier',
            'name' => 'testName'
        ];
        $token = new CsrfToken($settings['identifier'],'testToken');
        $this->service->injectCsrfTokenManager($this->getCsrfTokenManager($token));
        $this->assertEquals('<meta name="testName" content="testToken">',$this->service->getMetaTag($settings));

    }
}
