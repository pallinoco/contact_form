<?php

namespace Pallino\ContactForm\Tests\Unit\Service;

use Pallino\ContactForm\Service\MailGeneratorService;
use Pallino\ContactForm\Service\ValidationService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Mime\Address;
use TYPO3\CMS\Core\Mail\FluidEmail;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

class MailGeneratorServiceTest extends UnitTestCase
{
    /**
     * @var \PHPUnit\Framework\MockObject\MockObject|ValidationService
     */
    protected $service;

    protected function setUp(): void
    {
        parent::setUp();
        $this->service = $this->getAccessibleMock(MailGeneratorService::class, ['completeEmailSendingProcess', 'getEmailFluidObject']);
    }

    public function testSendEmailWithCorrectConfiguration()
    {
        $configuration = [
            'mail' => [
                'contactFormTemplate' => 'ContactFormCustomer',
                'subject' => [
                    'value' => 'testSubject',
                ],
                'sender' => [
                    'mail' => [
                        'field' => 'email',
                    ],
                    'name' => [
                        'value' => 'testSenderName'
                    ]
                ],
                'receiver' => [
                    'field' => 'email'
                ]
            ]
        ];
        $data = [
            'email' => 'test@test.it'
        ];
        $mailData = [
            'email' => 'test@test.it',
            'receiver' => 'test@test.it',
            'senderName' => 'testSenderName',
            'senderEmail' => 'test@test.it'
        ];
        $emailFluid = $this->getMockBuilder(FluidEmail::class)->disableOriginalConstructor()->onlyMethods(['to', 'from', 'subject', 'setTemplate', 'assignMultiple', 'format'])->getMock();
        $emailFluid->expects($this->once())->method('to')->with($data['email'])->willReturn($emailFluid);
        $address = new Address($data['email'], 'testSenderName');
        $emailFluid->expects($this->once())->method('from')->with($address)->willReturn($emailFluid);
        $emailFluid->expects($this->once())->method('subject')->with('testSubject')->willReturn($emailFluid);
        $emailFluid->expects($this->once())->method('setTemplate')->with('ContactFormCustomer')->willReturn($emailFluid);
        $emailFluid->expects($this->once())->method('assignMultiple')->with($mailData)->willReturn($emailFluid);
        $emailFluid->expects($this->once())->method('format')->with('html')->willReturn($emailFluid);
        $this->service->method('getEmailFluidObject')->willReturn($emailFluid);
        $this->service->setConfiguration($configuration);
        $this->service->expects($this->once())->method('completeEmailSendingProcess');
        $this->service->sendEmails($data);
    }
}
