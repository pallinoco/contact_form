<?php

namespace Pallino\ContactForm\Tests\Unit\Service;

use Pallino\ContactForm\Controller\ContactFormController;
use Pallino\ContactForm\Service\CsrfMetaTagManager;
use Pallino\ContactForm\Service\ValidationService;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;
use TYPO3\CMS\Extbase\Error\Error;
use TYPO3\CMS\Extbase\Error\Result;
use TYPO3\CMS\Extbase\Mvc\Response;
use TYPO3\CMS\Extbase\Validation\Validator\NotEmptyValidator;
use TYPO3\CMS\Extbase\Validation\Validator\StringLengthValidator;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

class ValidationServiceTest extends UnitTestCase
{
    /**
     * @var \PHPUnit\Framework\MockObject\MockObject|ValidationService
     */
    protected $service;

    protected $controller;

    protected function setUp(): void
    {
        parent::setUp();
        $this->service = $this->getAccessibleMock(ValidationService::class, ['getValidatorObject']);
    }

    /********************** TEST REQUIRED FEATURES *****************/

    protected function getConfigurationArray()
    {
        $formConfiguration['configuration']['name'] = ['required' => 1];
        $formConfiguration['configuration']['email'] = ['required' => 0];
        return $formConfiguration;
    }

    public function testIfMissingRequiredFieldsErrorGenerate()
    {
        $formConfiguration = $this->getConfigurationArray();
        $data = ['email' => 'testEmail'];
        $this->service->setConfiguration($formConfiguration);
        $this->assertFalse($this->service->isPresentRequiredFields($data));
        $this->assertEquals(['name'], $this->service->getMissingRequiredFields());
    }

    public function testIfAllFieldsIsPresentNoErrorGenerate()
    {
        $formConfiguration = $this->getConfigurationArray();
        $data = ['name' => 'testName'];
        $this->service->setConfiguration($formConfiguration);
        $this->assertTrue($this->service->isPresentRequiredFields($data));
        $this->assertEquals([], $this->service->getMissingRequiredFields());
    }

    public function testIfAllFieldsIsPresentButNoRequiredIsDefinedNoErrorGenerate()
    {
        $formConfiguration = $this->getConfigurationArray();
        $formConfiguration['configuration']['name']['required'] = 0;
        $data = [];
        $this->service->setConfiguration($formConfiguration);
        $this->assertTrue($this->service->isPresentRequiredFields($data));
        $this->assertEquals([], $this->service->getMissingRequiredFields());
    }

    public function testIfConfigurationIsMissingThrowAnException()
    {
        $formConfiguration = [];
        $data = [];
        $this->service->setConfiguration($formConfiguration);
        $this->expectException(\Exception::class);
        $this->service->isPresentRequiredFields($data);
    }


    /************************** TEST SANITIZED *****************************/

    protected function getSanitizedConfiguration()
    {
        $configuration = [
            'fields' => [
                'name',
                'surname',
                'email',
                'message'
            ]
        ];
        return $configuration;
    }

    public function testAllDataIsCorrectConfiguredThenAllDataIsSanitized()
    {
        $data = [
            'name' => 'testName',
            'surname' => 'testSurname',
            'email' => 'test@test.it',
            'message' => 'testMessage',
        ];
        $this->service->setConfiguration($this->getSanitizedConfiguration());
        $this->assertEquals($data, $this->service->getSanitizeFormFields($data));
    }

    public function testAllDataIsCorrectConfiguredButInConfigurationThereISMoreFieldsThenAllDataIsSanitized()
    {
        $data = [
            'name' => 'testName',
            'surname' => 'testSurname',
            'email' => 'test@test.it',
            'message' => 'testMessage',
        ];
        $configuration = $this->getSanitizedConfiguration();
        $configuration['fields'][] = 'extraField';
        $this->service->setConfiguration($configuration);
        $this->assertEquals($data, $this->service->getSanitizeFormFields($data));
    }

    public function testIsPresentFieldNotConfiguredThenArrayWillBeSanitized()
    {
        $data = [
            'name' => 'testName',
            'surname' => 'testSurname',
            'email' => 'test@test.it',
            'message' => 'testMessage',
            'extraField' => 'extraFieldTest'
        ];
        $expectedData = $data;
        unset($expectedData['extraField']);
        $this->service->setConfiguration($this->getSanitizedConfiguration());
        $this->assertEquals($expectedData, $this->service->getSanitizeFormFields($data));
    }

    /************************** VALIDATION FEATURES ***********************/

    public function testValidationFailedThenReturnAnError()
    {
        $configuration = [
            'configuration' => [
                'name' => [
                    'validators' => [
                        '1' => [
                            '_typoScriptNodeValue' => 'TYPO3\CMS\Extbase\Validation\Validator\StringLengthValidator',
                            'alias' => 'limit',
                            'minimum' => 5,
                            'maximum' => 7
                        ],
                        '2' => 'TYPO3\CMS\Extbase\Validation\Validator\NotEmptyValidator'
                    ],
                    'required' => 1
                ]
            ]
        ];
        $this->service->method('getValidatorObject')->will($this->returnCallback(array($this, 'catchValidatorsWithErrors')));
        $this->service->setConfiguration($configuration);
        $this->assertFalse($this->service->evaluateForm(['name' => 'nameTest']));
        $errors = [
            'name' => [
                'limit' => 'string validation Error'
            ]
        ];
        $this->assertEquals($errors,$this->service->getErrors());
    }

    public function testValidationSuccessThenNoErrorReturned()
    {
        $configuration = [
            'configuration' => [
                'name' => [
                    'validators' => [
                        '1' => [
                            '_typoScriptNodeValue' => 'TYPO3\CMS\Extbase\Validation\Validator\StringLengthValidator',
                            'alias' => 'limit',
                            'minimum' => 5,
                            'maximum' => 7
                        ],
                        '2' => 'TYPO3\CMS\Extbase\Validation\Validator\NotEmptyValidator'
                    ],
                    'required' => 1
                ]
            ]
        ];
        $this->service->method('getValidatorObject')->will($this->returnCallback(array($this, 'catchValidatorsWithoutErrors')));
        $this->service->setConfiguration($configuration);
        $this->assertTrue($this->service->evaluateForm(['name' => 'nameTest']));
        $this->assertEquals([],$this->service->getErrors());
    }

    public function catchValidatorsWithErrors()
    {
        $arguments = func_get_args();
        switch ($arguments[0]) {
            case 'TYPO3\CMS\Extbase\Validation\Validator\StringLengthValidator':
                $result = new Result();
                $result->addError(new Error('string validation Error', 100));
                return $this->getValidator($arguments[0],$result);
                break;
            case 'TYPO3\CMS\Extbase\Validation\Validator\NotEmptyValidator':
                $result = new Result();
                return $this->getValidator($arguments[0],$result);
        }
    }

    public function catchValidatorsWithoutErrors()
    {
        $arguments = func_get_args();
        switch ($arguments[0]) {
            case 'TYPO3\CMS\Extbase\Validation\Validator\StringLengthValidator':
                $result = new Result();
                return $this->getValidator($arguments[0],$result);
                break;
            case 'TYPO3\CMS\Extbase\Validation\Validator\NotEmptyValidator':
                $result = new Result();
                return $this->getValidator($arguments[0],$result);
        }
    }

    protected function getValidator(string $validatorName, Result $result)
    {
        $validatorObject = $this->getMockBuilder($validatorName)->onlyMethods(['validate'])->getMock();
        $validatorObject->method('validate')->willReturn($result);
        return $validatorObject;
    }

}
