<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "t3palcore".
 *
 * Auto generated 10-01-2013 17:54
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = [
    'title' => 'Pallino contact form',
    'description' => 'Create a contact or generic form callable via api json with csrf protection',
    'author' => 'Federico Bernardin',
    'author_email' => 'federico.bernardin@pallino.it',
    'category' => 'frontend',
    'author_company' => 'Pallino & Co.',
    'shy' => '',
    'priority' => '',
    'module' => '',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => 0,
    'createDirs' => '',
    'modify_tables' => '',
    'clearCacheOnLoad' => 1,
    'lockType' => '',
    'version' => '1.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '10.4.0-10.99.0'
        ],
        'suggests' => [
        ],
    ],
    'suggests' => [
    ],
];
