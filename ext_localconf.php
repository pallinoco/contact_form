<?php
defined('TYPO3_MODE') || die();

call_user_func(
    function () {
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Pallino.ContactForm',
            'Pi1',
            [
                'ContactForm' => 'sendContact,makeCsrfTag',
            ],
            // non-cacheable actions
            [
                'ContactForm' => 'sendContact,makeCsrfTag',
            ]
        );
    }
);